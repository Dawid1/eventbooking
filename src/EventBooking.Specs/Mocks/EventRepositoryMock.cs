﻿using EventBooking.Server.Events;
using System;
using System.Collections.Generic;
using EventBooking.Server.Seats;
using System.Linq;
using EventBooking.Server.ConferenceRooms;

namespace EventBooking.Specs.Mocks
{
    public class EventRepositoryMock : IEventRepository
    {
        private readonly InMemoryTestDb _db;
        public EventRepositoryMock()
        {
            _db = InMemoryTestDb.Instance;
        }

        public ConferenceRoom ConferenceRoomGet(Guid eventId, Guid roomId)
        {
            return _db.Events.FirstOrDefault(e => e.Id == eventId).Rooms.FirstOrDefault(r => r.Id == roomId);
        }

        public List<Event> GetAll()
        {
            return _db.Events;
        }

        public Event GetEvent(Func<Event, bool> predicate)
        {
            return _db.Events.FirstOrDefault(predicate);
        }

        public Seat GetSeatById(Guid eventId, Guid conferenceRoomId, Guid seatId)
        {
            var seatFromDb = GetAll()?.FirstOrDefault(e => e.Id == eventId)?.Rooms.FirstOrDefault(r => r.Id == conferenceRoomId)?.Seats?.FirstOrDefault(s => s.Id == seatId);
            if (seatFromDb == null)
                throw new Exception("Seat not found");

            return seatFromDb;
        }

        public void SaveChanges()
        {
            
        }
    }
}
