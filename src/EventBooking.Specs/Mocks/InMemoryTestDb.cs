﻿using EventBooking.Server.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBooking.Specs.Mocks
{
    public sealed class InMemoryTestDb
    {
        private static volatile InMemoryTestDb instance;
        private static object syncRoot = new Object();

        public List<Event> Events { get; private set; }

        private InMemoryTestDb()
        {
            Events = new List<Event>();
        }

        public static InMemoryTestDb Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new InMemoryTestDb();
                        }
                    }
                }
                return instance;
            }
        }

        public void ClearDb()
        {
            Events.Clear();
        }

        public void AddEvent(Event eventToAdd)
        {
            Events.Add(eventToAdd);
        }
    }
}
