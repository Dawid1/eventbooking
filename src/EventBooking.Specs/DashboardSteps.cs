﻿using EventBooking.Server.Events;
using EventBooking.Specs.Mocks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using TechTalk.SpecFlow;

namespace EventBooking.Specs
{
    [Binding]
    public class DashboardSteps
    {
        private IWebDriver _driver;

        private IEventService _service = SetupFeature.Container.GetInstance<IEventService>();
        private IEventLogic _logic = SetupFeature.Container.GetInstance<IEventLogic>();

        private readonly DashboardStepsContext _ctx;

        public DashboardSteps(DashboardStepsContext ctx)
        {
            _ctx = ctx;
        }

        [Given(@"One event exists")]
        public void GivenOneEventExists()
        {
            var event1 = new Event("My sample event");
            _logic.CreateConferenceRoom(event1, 7, 7, "Blue");
            InMemoryTestDb.Instance.AddEvent(event1);
        }
        
        [When(@"I browse dashboard page")]
        public void WhenIBrowseDashboardPage()
        {
            //_driver = new FirefoxDriver();
            //_driver.Navigate().GoToUrl("http://localhost:8000/");
            _ctx.AvailableEvents = _service.GetDashboardDto().EventNumbers;

        }
        
        [Then(@"The blue thumbnail should display (.*)")]
        public void ThenTheBlueThumbnailShouldDisplay(int p0)
        {
            //IWebElement divWithEventNumber = _driver.FindElement(By.Id("dashboardEventNumbers"));

            //IWebElement h2 = divWithEventNumber.FindElement(By.TagName("h2"));

            //Assert.AreEqual("1", h2.Text);

            Assert.AreEqual(1, _ctx.AvailableEvents);

        }
    }
}
