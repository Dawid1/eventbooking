﻿using EventBooking.Server.Events;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBooking.Specs
{
    public class DashboardStepsContext
    {
        public int AvailableEvents { get; set; }

        public int AvailableSeats { get; set; }
    }
}
