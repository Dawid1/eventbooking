﻿using EventBooking.Server;
using EventBooking.Specs.IoC;
using StructureMap;
using TechTalk.SpecFlow;

namespace EventBooking.Specs
{
    [Binding]
    public class SetupFeature
    {
        public static IContainer Container;

        [BeforeFeature("setup_feature")]
        public static void BeforeAppFeature()
        {
            Container = new Container(x =>
            {
                x.AddRegistry<EventBookingServerRegistry>();
                x.AddRegistry<EventBookingSpecsRegistry>();
            });
        }
    }
}
