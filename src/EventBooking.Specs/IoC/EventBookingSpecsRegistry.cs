﻿using EventBooking.Server.Events;
using EventBooking.Specs.Mocks;
using StructureMap;

namespace EventBooking.Specs.IoC
{
    public class EventBookingSpecsRegistry : Registry
    {
        public EventBookingSpecsRegistry()
        {
            For<IEventRepository>().Use<EventRepositoryMock>();

        }
    }
}
