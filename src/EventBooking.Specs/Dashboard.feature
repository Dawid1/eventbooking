﻿@setup_feature
Feature: Dashboard
	In order to browse dashboard page
	As a user
	I want to be able to see all available events and all available seats for all events and conference rooms


Scenario: Seeing available events
	Given One event exists
	When I browse dashboard page
	Then The blue thumbnail should display 1
