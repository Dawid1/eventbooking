﻿using EventBooking.Server.Db;

namespace EventBooking.Server
{
    public abstract class BaseEventBookingRepository
    {
        protected readonly EventBookingContext _ctx;

        public BaseEventBookingRepository(EventBookingContext ctx)
        {
            _ctx = ctx;
        }
    }
}
