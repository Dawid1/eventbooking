﻿using EventBooking.Server.Db;
using StructureMap;
using StructureMap.Graph;

namespace EventBooking.Server
{
    public class EventBookingServerRegistry : Registry
    {
        public EventBookingServerRegistry()
        {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
            });

            For<EventBookingContext>().Use<EventBookingContext>().ContainerScoped();
        }
    }
}
