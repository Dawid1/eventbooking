﻿using EventBooking.Server.ConferenceRooms;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventBooking.Server.Seats
{
    public class Seat
    {
        public Seat()
        {

        }

        public Seat(int seatNumber, bool available, int rowNumber, int columnNumber)
        {
            SeatNumber = seatNumber;
            Available = available;
            Row = rowNumber;
            Column = columnNumber;
            Id = Guid.NewGuid();
        }

        public int SeatNumber { get; set; }

        public bool Available { get; set; }

        public int Row { get; set; }

        public int Column { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public virtual ConferenceRoom ConferenceRoom { get; set; }

    }
}
