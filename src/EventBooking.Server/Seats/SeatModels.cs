﻿using System;

namespace EventBooking.Server.Seats
{
    public class SeatDataMessage
    {
        public MessageType MessageType { get; set; }

        public Guid SeatId { get; set; }

        public Guid RoomId { get; set; }

        public Guid EventId { get; set; }

        public string ResponseMessage { get; set; }

        public bool DataProcessedSuccessfully { get; set; }
    }

    public enum MessageType
    {
        Booking = 1,
        Releasing = 2
    }
}
