namespace EventBooking.Server.Migrations
{
    using ConferenceRooms;
    using Db;
    using Events;
    using Seats;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EventBooking.Server.Db.EventBookingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EventBooking.Server.Db.EventBookingContext context)
        {
           
            var eventAsp = new Event("ASP.NET Core Conference");
            var eventMicroservices = new Event("Microservices");

            context.Events.AddOrUpdate(eventAsp);
            context.Events.AddOrUpdate(eventMicroservices);

            context.SaveChanges();

            CreateEventConferenceRoom(context, eventAsp, 10, 10, "Blue");
            CreateEventConferenceRoom(context, eventAsp, 4, 7, "Yellow");

            CreateEventConferenceRoom(context, eventMicroservices, 5, 5, "Mars");

            context.SaveChanges();
        }

        private void CreateEventConferenceRoom(EventBookingContext context, Event currentEvent, int rows, int columns, string name)
        {
            var conferenceRoom = new ConferenceRoom();
            conferenceRoom.Rows = rows;
            conferenceRoom.Columns = columns;
            conferenceRoom.Name = name;
            conferenceRoom.Id = Guid.NewGuid();

            for (int i = 1; i <= rows; i++)
            {
                for (int j = 1; j <= columns; j++)
                {
                    var seat = new Seat(((i - 1) * columns) + j, true, i, j);
                    conferenceRoom.Seats.Add(seat);
                    context.Seats.AddOrUpdate(seat);
                }
            }
            currentEvent.Rooms.Add(conferenceRoom);
            conferenceRoom.Event = currentEvent;
            context.ConferenceRooms.AddOrUpdate(conferenceRoom);
            context.SaveChanges();
        }
    }
}
