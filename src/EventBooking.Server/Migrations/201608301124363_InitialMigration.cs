namespace EventBooking.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConferenceRoom",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Rows = c.Int(nullable: false),
                        Columns = c.Int(nullable: false),
                        Event_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Event", t => t.Event_Id)
                .Index(t => t.Event_Id);
            
            CreateTable(
                "dbo.Event",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Active = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Seat",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SeatNumber = c.Int(nullable: false),
                        Available = c.Boolean(nullable: false),
                        Row = c.Int(nullable: false),
                        Column = c.Int(nullable: false),
                        ConferenceRoom_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConferenceRoom", t => t.ConferenceRoom_Id)
                .Index(t => t.ConferenceRoom_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Seat", "ConferenceRoom_Id", "dbo.ConferenceRoom");
            DropForeignKey("dbo.ConferenceRoom", "Event_Id", "dbo.Event");
            DropIndex("dbo.Seat", new[] { "ConferenceRoom_Id" });
            DropIndex("dbo.ConferenceRoom", new[] { "Event_Id" });
            DropTable("dbo.Seat");
            DropTable("dbo.Event");
            DropTable("dbo.ConferenceRoom");
        }
    }
}
