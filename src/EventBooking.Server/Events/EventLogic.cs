﻿using EventBooking.Server.ConferenceRooms;
using EventBooking.Server.Seats;
using System;
using System.Linq;

namespace EventBooking.Server.Events
{
    public interface IEventLogic
    {
        void BookSeat(Guid eventId, Guid conferenceRoomId, Guid seatId);

        void ReleaseSeat(Guid eventId, Guid conferenceRoomId, Guid seatId);

        ConferenceRoom CreateConferenceRoom(Event currentEvent, int rows, int columns, string name);

        Seat CreateConferenceRoomSeat(ConferenceRoom room, int seatNumber, bool available, int row, int column);
    }

    public class EventLogic : IEventLogic
    {
        private readonly IEventRepository _eventRepository;

        public EventLogic(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public void BookSeat(Guid eventId, Guid conferenceRoomId, Guid seatId)
        {
            var seatFromDb = _eventRepository.GetSeatById(eventId, conferenceRoomId, seatId);

            if (seatFromDb.Available)
                seatFromDb.Available = false;
            else
                throw new Exception("Seat has already been booked");

        }

        public void ReleaseSeat(Guid eventId, Guid conferenceRoomId, Guid seatId)
        {
            var seatFromDb = _eventRepository.GetSeatById(eventId, conferenceRoomId, seatId);

            if (!seatFromDb.Available)
                seatFromDb.Available = true;
            else
                throw new Exception("Seat has already been released");
        }

        public ConferenceRoom CreateConferenceRoom(Event currentEvent, int rows, int columns, string name)
        {
            var conferenceRoom = new ConferenceRoom();
            conferenceRoom.Rows = rows;
            conferenceRoom.Columns = columns;
            conferenceRoom.Name = name;
            conferenceRoom.Id = Guid.NewGuid();

            for (int i = 1; i <= rows; i++)
            {
                for (int j = 1; j <= columns; j++)
                {
                    CreateConferenceRoomSeat(conferenceRoom, ((i - 1) * columns) + j, true, i, j);

                    //conferenceRoom.Seats.Add(new Seat(((i - 1) * columns) + j, true, i, j));
                }
            }
            currentEvent.Rooms.Add(conferenceRoom);
            conferenceRoom.Event = currentEvent;

            return conferenceRoom;
        }

        public Seat CreateConferenceRoomSeat(ConferenceRoom room, int seatNumber, bool available, int row, int column)
        {
            var seat = new Seat(seatNumber, available, row, column);
            room.Seats.Add(seat);

            return seat;
        }
    }
}
