﻿using System;
using System.Collections.Generic;
using EventBooking.Server.ConferenceRooms;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventBooking.Server.Events
{
    public class Event
    {
        public Event()
        {

        }

        public Event(string name)
        {
            Name = name;
            CreatedDate = DateTime.Now;
            Active = true;
            Rooms = new List<ConferenceRoom>();
            Id = Guid.NewGuid();
        }

        public string Name { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public virtual ICollection<ConferenceRoom> Rooms { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
    }
}
