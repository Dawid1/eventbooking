﻿using EventBooking.Server.ConferenceRooms;
using EventBooking.Server.Seats;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EventBooking.Server.Events
{
    public class DashboardDto
    {
        public int EventNumbers { get; set; }
        public int AvailableSeats { get; set; }
    }

    public class EventDto
    {
        public EventDto(Event currentEvent)
        {
            Name = currentEvent.Name;
            Id = currentEvent.Id;
            Rooms = currentEvent.Rooms.Select(r => new ConferenceRoomDto(r)).ToList();
        }

        public string Name { get; set; }

        public Guid Id { get; set; }
        public List<ConferenceRoomDto> Rooms { get; set; }
    }

    public class ConferenceRoomDto
    {
        public ConferenceRoomDto()
        {

        }

        public ConferenceRoomDto(ConferenceRoom room)
        {
            Name = room.Name;
            Rows = room.Rows;
            Columns = room.Columns;
            Id = room.Id;
            Seats = room.Seats.Select(s => new SeatDto(s)).ToList();
            AvailableSeats = room.Seats.Where(s => s.Available).Count();
        }

        public string Name { get; set; }
        public int Rows { get; set; }
        public int Columns { get; set; }
        public List<SeatDto> Seats { get; set; }
        public Guid Id { get; set; }
        public int AvailableSeats { get; set; }
        public Guid EventId { get; set; }
    }

    public class SeatDto
    {
        public SeatDto()
        {

        }

        public SeatDto(Seat seat)
        {
            SeatNumber = seat.SeatNumber;
            Available = seat.Available;
            Row = seat.Row;
            Column = seat.Column;
            Id = seat.Id;
        }

        public int SeatNumber { get; set; }

        public bool Available { get; set; }

        public int Row { get; set; }

        public int Column { get; set; }

        public Guid Id { get; set; }
    }

    public class UpdateSeatDto
    {

    }
}
