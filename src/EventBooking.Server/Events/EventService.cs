﻿using EventBooking.Server.Seats;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EventBooking.Server.Events
{
    public interface IEventService
    {
        List<EventDto> GetEvents();
        EventDto GetEventByName(string eventName);

        DashboardDto GetDashboardDto();

        void UpdateSeat(Guid eventId, Guid roomId, Seat seat);

        SeatDataMessage HandleSeatAvaibilityMessage(string receivedMessage);

        void CreateConferenceRoom(ConferenceRoomDto room);

        ConferenceRoomDto ConferenceRoomGet(Guid eventId, Guid roomId);
    }

    public class EventService : IEventService
    {
        private readonly IEventRepository _repository;
        private readonly IEventLogic _logic;

        public EventService(IEventRepository repository, IEventLogic logic)
        {
            _repository = repository;
            _logic = logic;
        }

        public DashboardDto GetDashboardDto()
        {
            var events = _repository.GetAll();
            var allSeats = 0;
            events.ForEach(e => e.Rooms.ToList().ForEach(r => allSeats += r.Seats.Where(s => s.Available).Count()));

            return new DashboardDto { EventNumbers = events.Count(), AvailableSeats = allSeats };
        }

        public EventDto GetEventByName(string eventName)
        {
            return new EventDto(_repository.GetEvent(e=>e.Name == eventName));
        }

        public List<EventDto> GetEvents()
        {
            return _repository.GetAll().Select(e=>new EventDto(e)).ToList();
        }

        public void UpdateSeat(Guid eventId, Guid roomId, Seat seat)
        {
            if (seat == null)
                throw new Exception("seat can not be null");

            if (seat.Available)
                _logic.ReleaseSeat(eventId, roomId, seat.Id);
            else
                _logic.BookSeat(eventId, roomId, seat.Id);
        }

        public SeatDataMessage HandleSeatAvaibilityMessage(string receivedString)
        {
            var responseMessage = new SeatDataMessage() { DataProcessedSuccessfully = true };

            try
            {
                var receivedMessage = JsonConvert.DeserializeObject<SeatDataMessage>(receivedString);
                if (receivedMessage.SeatId == null || receivedMessage.EventId == null || receivedMessage.RoomId == null)
                {
                    responseMessage.DataProcessedSuccessfully = false;
                    responseMessage.ResponseMessage = "Not all data sent correctly";
                }
                else
                {
                    responseMessage.EventId = receivedMessage.EventId;
                    responseMessage.RoomId = receivedMessage.RoomId;
                    responseMessage.SeatId = receivedMessage.SeatId;
                    responseMessage.MessageType = receivedMessage.MessageType;

                    switch (receivedMessage.MessageType)
                    {
                        case MessageType.Booking:
                            _logic.BookSeat(receivedMessage.EventId, receivedMessage.RoomId, receivedMessage.SeatId);
                            _repository.SaveChanges();
                            break;
                        case MessageType.Releasing:
                            _logic.ReleaseSeat(receivedMessage.EventId, receivedMessage.RoomId, receivedMessage.SeatId);
                            _repository.SaveChanges();
                            break;
                        default:
                            responseMessage.DataProcessedSuccessfully = false;
                            responseMessage.ResponseMessage = "Operation not allowed";
                            break;

                    }
                }
            }
            catch (ArgumentNullException e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message);
                responseMessage.DataProcessedSuccessfully = false;
                responseMessage.ResponseMessage = "Input data are absent.";
            }
            catch (ArgumentException e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message);
                responseMessage.DataProcessedSuccessfully = false;
                responseMessage.ResponseMessage = "Input data had an incorrect format.";
            }
            catch (InvalidOperationException e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message);
                responseMessage.DataProcessedSuccessfully = false;
                responseMessage.ResponseMessage = "Data handling error on the server.";
            }

            return responseMessage;
        }

        public void CreateConferenceRoom(ConferenceRoomDto room)
        {
            if (room == null || room.Seats == null || room.EventId == null)
                throw new Exception("Invalid data");

            var currentEvent = _repository.GetEvent(e=> e.Id == room.EventId);
            if (currentEvent == null)
                throw new Exception("Invalid event id");

            var conference = _logic.CreateConferenceRoom(currentEvent, 0, 0, room.Name);

            conference.Rows = room.Rows;
            conference.Columns = room.Columns;

            foreach(var seat in room.Seats)
            {
                var seatPers = _logic.CreateConferenceRoomSeat(conference, seat.SeatNumber, seat.Available, seat.Row, seat.Column);
            }

            _repository.SaveChanges();
        }

        public ConferenceRoomDto ConferenceRoomGet(Guid eventId, Guid roomId)
        {
            return new ConferenceRoomDto(_repository.ConferenceRoomGet(eventId, roomId));
        }
    }
}
