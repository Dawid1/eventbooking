﻿using EventBooking.Server.Db;
using EventBooking.Server.Seats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using EventBooking.Server.ConferenceRooms;

namespace EventBooking.Server.Events
{
    public interface IEventRepository
    {
        List<Event> GetAll();

        Event GetEvent(Func<Event, bool> predicate);

        Seat GetSeatById(Guid eventId, Guid conferenceRoomId, Guid seatId);

        void SaveChanges();

        ConferenceRoom ConferenceRoomGet(Guid eventId, Guid roomId);
    }

    public class EventRepository : BaseEventBookingRepository, IEventRepository
    {
        public EventRepository(EventBookingContext ctx) : base(ctx)
        {

        }

        public ConferenceRoom ConferenceRoomGet(Guid eventId, Guid roomId)
        {
            return _ctx.ConferenceRooms.Include(r=>r.Seats).FirstOrDefault(r => r.Id == roomId && r.Event.Id == eventId);
        }

        public List<Event> GetAll()
        {
            return _ctx.Events.Include(e=>e.Rooms.Select(r=>r.Seats)).ToList();
        }

        public Event GetEvent(Func<Event,bool> predicate)
        {
            return _ctx.Events.Include(e => e.Rooms.Select(r => r.Seats)).FirstOrDefault(predicate);
        }

        public Seat GetSeatById(Guid eventId, Guid conferenceRoomId, Guid seatId)
        {
            var seatFromDb = _ctx.Seats.FirstOrDefault(s => s.Id == seatId);
            if (seatFromDb == null || seatFromDb.ConferenceRoom == null || seatFromDb.ConferenceRoom.Event == null || 
                seatFromDb.ConferenceRoom.Id != conferenceRoomId || seatFromDb.ConferenceRoom.Event.Id != eventId)
                throw new Exception("Seat not found");

            return seatFromDb;
        }

        public void SaveChanges()
        {
            _ctx.SaveChanges();
        }
    }
}
