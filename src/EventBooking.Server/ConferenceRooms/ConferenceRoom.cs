﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventBooking.Server.Seats;
using EventBooking.Server.Events;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventBooking.Server.ConferenceRooms
{
    public class ConferenceRoom
    {
        public ConferenceRoom()
        {
            Seats = new List<Seat>();
        }

        public ConferenceRoom(int rows, int columns, string name)
        {
            Rows = rows;
            Columns = columns;
            Name = name;
            Id = Guid.NewGuid();
            Seats = new List<Seat>();
        }

        public string Name { get; set; }

        public int Rows { get; set; }
        public int Columns { get; set; }
        public List<Seat> Seats { get; set; }

        public virtual Event Event { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        //public int AvailableSeats { get { return Seats.Where(s => s.Available).Count(); } }

        //public void BookSeat(int seatNumber)
        //{
        //    var seat = Seats.FirstOrDefault(s => s.SeatNumber == seatNumber);
        //    if (seat == null)
        //        throw new Exception("Invalid seat number");

        //    if (!seat.Available)
        //        throw new Exception("Seat is already booked");

        //    seat.Available = false;
        //}

        //public void ReleaseSeat(int seatNumber)
        //{
        //    var seat = Seats.FirstOrDefault(s => s.SeatNumber == seatNumber);
        //    if (seat == null)
        //        throw new Exception("Invalid seat number");

        //    if (seat.Available)
        //        throw new Exception("Seat is already released");

        //    seat.Available = true;
        //    //Seats.Remove(seat);
        //    //Seats.Add(new Seat(seat.SeatNumber, true, seat.Row, seat.Column));
        //}
    }
}
