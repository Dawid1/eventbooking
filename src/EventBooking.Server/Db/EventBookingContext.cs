﻿using EventBooking.Server.ConferenceRooms;
using EventBooking.Server.Events;
using EventBooking.Server.Seats;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace EventBooking.Server.Db
{
    [DbConfigurationType(typeof(CodeConfig))]
    public class EventBookingContext : DbContext
    {
        public EventBookingContext()
            : base("Server=(localdb)\\MSSQLLocalDB;Database=EventBooking;Trusted_Connection=True;MultipleActiveResultSets=true;")
        {
            Database.SetInitializer<EventBookingContext>(new CreateDatabaseIfNotExists<EventBookingContext>());
        }

        public DbSet<Event> Events { get; set; }
        public DbSet<ConferenceRoom> ConferenceRooms { get; set; }
        public DbSet<Seat> Seats { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

    public class CodeConfig : DbConfiguration
    {
        public CodeConfig()
        {
            SetProviderServices("System.Data.SqlClient",
                System.Data.Entity.SqlServer.SqlProviderServices.Instance);
        }
    }
}
