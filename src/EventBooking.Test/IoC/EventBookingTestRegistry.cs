﻿using EventBooking.Server.Events;
using EventBooking.Test.Mocks;
using StructureMap;

namespace EventBooking.Test.IoC
{
    public class EventBookingTestRegistry : Registry
    {
        public EventBookingTestRegistry()
        {
            For<IEventRepository>().Use<EventRepositoryMock>();

        }
    }
}
