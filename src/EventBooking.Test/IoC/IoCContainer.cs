﻿using EventBooking.Server;
using StructureMap;

namespace EventBooking.Test.IoC
{
    public class IoCContainer
    {
        public static IContainer Container;

        public static void InitializeContainer()
        {
            Container = new Container(x =>
            {
                x.AddRegistry<EventBookingServerRegistry>();
                x.AddRegistry<EventBookingTestRegistry>();
            });
        }
        
    }
}
