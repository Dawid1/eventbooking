﻿using EventBooking.Server.Events;
using EventBooking.Server.Seats;
using EventBooking.Test.IoC;
using EventBooking.Test.Mocks;
using NUnit.Framework;
using System;
using System.Linq;

namespace EventBooking.Test.Integration
{
    [TestFixture]
    public class EventServiceTest
    {
        private IEventService _service;
        private IEventLogic _logic;

        [OneTimeSetUp]
        public void Initialize()
        {
            IoCContainer.InitializeContainer();
            _service = IoCContainer.Container.GetInstance<IEventService>();
            _logic = IoCContainer.Container.GetInstance<IEventLogic>();
        }

        [SetUp]
        public void Init()
        {
            InMemoryTestDb.Instance.ClearDb();
        }

        [Test]
        public void ShouldBeAbleToGetDashboardData()
        {
            //Arrange
            var event1 = new Event("My sample event");
            _logic.CreateConferenceRoom(event1, 7, 7, "Blue");
            InMemoryTestDb.Instance.AddEvent(event1);

            //Act
            var dashboardDto = _service.GetDashboardDto();

            //Assert
            Assert.IsNotNull(dashboardDto);
            Assert.AreEqual(dashboardDto.EventNumbers, 1);
            Assert.AreEqual(dashboardDto.AvailableSeats, 49);
        }

        [Test]
        public void ShouldGetEventByName()
        {
            //Arrange
            var eventName = "My sample name";
            var event1 = new Event(eventName);
            _logic.CreateConferenceRoom(event1, 7, 7, "Blue");
            InMemoryTestDb.Instance.AddEvent(event1);

            //Act
            var eventData = _service.GetEventByName(eventName);

            //Assert
            Assert.IsNotNull(eventData);
            Assert.AreEqual(eventData.Name, eventName);
            Assert.AreEqual(eventData.Rooms.Count, 1);
        }

        [Test]
        public void ShouldBeAbleToBookSeat()
        {
            //Arrange
            var eventName = "My sample name";
            var event1 = new Event(eventName);
            _logic.CreateConferenceRoom(event1, 7, 7, "Blue");
            InMemoryTestDb.Instance.AddEvent(event1);

            //Act
            _service.UpdateSeat(event1.Id, event1.Rooms.First().Id, event1.Rooms.First().Seats.Where(s => s.SeatNumber == 21).Select(s => new Seat { Id = s.Id, Available = false, Column = s.Column, Row = s.Row, SeatNumber = s.SeatNumber }).FirstOrDefault());

            //Assert
            Assert.AreEqual(InMemoryTestDb.Instance.Events.First().Rooms.First().Seats.FirstOrDefault(s=>s.SeatNumber == 21).Available, false);
        }

        [Test]
        public void ShouldNotBeAbleToBookUnknownSeat()
        {
            //Arrange
            var eventName = "My sample name";
            var event1 = new Event(eventName);
            _logic.CreateConferenceRoom(event1, 7, 7, "Blue");
            InMemoryTestDb.Instance.AddEvent(event1);

            //Act
            var exception = Assert.Throws<Exception>(() => _service.UpdateSeat(event1.Id, event1.Rooms.First().Id, event1.Rooms.First().Seats.Where(s => s.SeatNumber == 21).Select(s => new Seat { Id = new Guid(), Available = false, Column = s.Column, Row = s.Row, SeatNumber = s.SeatNumber }).FirstOrDefault()));

            //Assert
            Assert.AreEqual("Seat not found", exception.Message);
        }
    }
}
