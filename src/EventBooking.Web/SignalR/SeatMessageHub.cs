﻿using EventBooking.Server.Events;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;

namespace EventBooking.Web.SignalR
{
    public class SeatMessageHub : Hub
    {
        private readonly IEventService _eventService;
        public SeatMessageHub(IEventService eventService)
        {
            _eventService = eventService;
        }

        public void HandleSeatMessage(string receivedString)
        {
            var response = _eventService.HandleSeatAvaibilityMessage(receivedString);

            if (response.DataProcessedSuccessfully)
            {
                Clients.All.handleSeatMessage(JsonConvert.SerializeObject(response));
            }
            else
            {
                Clients.Caller.handleSeatMessage(JsonConvert.SerializeObject(response));
            }
        }
    }
}
