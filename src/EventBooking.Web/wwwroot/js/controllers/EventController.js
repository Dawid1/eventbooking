﻿'use strict';
app.controller('EventController', function EventController($scope, $rootScope, dialogService, eventData, ngDialog) {
    $scope.events = [];
    $scope.currentEvent = {};
    $scope.currentRoom = {};
    $scope.sortorder = 'name';
    $scope.modalShown = false;
    $rootScope.modalContent = "";
    $scope.eventHasChanged = false;
    $scope.currentSeat = {};

    eventData.getEvents()
        .success(function (events) {
            $scope.events = events;
            if (events.length > 0) {
                $scope.currentEvent = events[0];
                if (events[0].rooms.length > 0) {
                    $scope.currentRoom = events[0].rooms[0];
                }
            }
        });

    $scope.showCurrentEvent = function (event) {
        $scope.currentEvent = event;
        $scope.currentRoom = event.rooms[0];
    };

    $scope.setModalContent = function (content) {
        dialogService.modalContent = content;
    };

    $scope.isActive = function (event) {
        if (event === $scope.currentEvent) {
            return "active";
        }

        return "inactive";
    };

    $scope.showRoom = function (room) {
        $scope.currentRoom = room;
    };

    $scope.range = _.range;

    $scope.getSeatClass = function (row, column) {
        var currentSeat = $scope.getCurrentSeat(column, row);
        if (currentSeat != null) {

            return currentSeat.available ? "seat-available" : "seat-booked";

        }
        return "not-defined";
        
    };

    $scope.getSeatNumber = function (row, column) {
        var currentSeat = $scope.getCurrentSeat(column, row);
        if (currentSeat) {

            return currentSeat.seatNumber;

        }
        return "";

    };

    $scope.showConfirmDialog = function(row, column){

        var currentSeat = $scope.getCurrentSeat(column, row);

        $scope.setModalContent("");

        if (currentSeat) {

            if (currentSeat.available) {
                $scope.setModalContent("You ara booking a seat number " + currentSeat.seatNumber + ". Please confirm.");
            }
            else {
                $scope.setModalContent("You ara releasing a seat number " + currentSeat.seatNumber + ". Please confirm.");
            }

        }
        else {
            $scope.setModalContent("Seat not available");
        }

        ngDialog.openConfirm({
            template: '/templates/confirmDialog.html',
            className: 'ngdialog-theme-default'
        }).then(function () {
            if (currentSeat) {
                $scope.sendSeatDataMessage(currentSeat, currentSeat.available ? 1 : 2);
            }
        }, function () {
            
        });        
    };

    $scope.getCurrentSeat = function(column, row) {
        var room = $scope.currentRoom;
        var allColumns = room.columns;
        var currentSeat = null;
        var count = room.seats.length;
        for (var i = 0; i < count; i++) {
            if (room.seats[i].column == column && room.seats[i].row == row) {
                currentSeat = room.seats[i];
                break;
            }
        }
        
        return currentSeat != null ? currentSeat : null;
    }

    $scope.sendSeatDataMessage = function (seat, messageType) {

        // Create the new message for sending.
        var seatDataMessage = new Object();

        // Set message type.
        seatDataMessage.MessageType = messageType;

        var room = $scope.currentRoom;
        var event = $scope.currentEvent;

        // Set message data.
        seatDataMessage.EventId = event.id;
        seatDataMessage.RoomId = room.id;
        seatDataMessage.SeatId = seat.id;

        // Send data to server.
        seatMessageHub.server.handleSeatMessage(JSON.stringify(seatDataMessage));
    };

    // Method which receives data.
    seatMessageHub.client.handleSeatMessage = function (message) {
        // Method which handles messages.
        $scope.receivedMessageHandler(message);
    };


    $scope.receivedMessageHandler = function (seatDataMessageJsonString) {
        var seatDataMessage = JSON.parse(seatDataMessageJsonString);
        if (seatDataMessage.DataProcessedSuccessfully) {

            var event = _.findWhere($scope.events, {id: seatDataMessage.EventId});
            if(event && event.rooms) {
                var room = _.findWhere(event.rooms, { id: seatDataMessage.RoomId });

                if (room && room.seats) {
                    var seat = _.findWhere(room.seats, { id: seatDataMessage.SeatId });

                    if (seat) {
                        switch (seatDataMessage.MessageType) {
                            case 1: // Booking
                                seat.available = false;
                                room.availableSeats -= 1;
                                break;
                            case 2: // Releasing.
                                seat.available = true;
                                room.availableSeats += 1;
                                break;
                            default:
                                return;
                        }
                        $scope.$apply();

                        //calback goes here
                        $scope.setModalContent("Operation has been done successfuly");
                        ngDialog.open({
                            template: '/templates/resultDialog.html',
                            className: 'ngdialog-theme-default'
                        });
                    }
                    else {
                        $scope.setModalContent("Seat not found !");
                        ngDialog.open({
                            template: '/templates/resultDialog.html',
                            className: 'ngdialog-theme-default'
                        });
                    }
                }
                else {
                    $$scope.setModalContent("Room not found !");
                    ngDialog.open({
                        template: '/templates/resultDialog.html',
                        className: 'ngdialog-theme-default'
                    });
                }
            }
            else {
                $scope.setModalContent("Event not found !");
                ngDialog.open({
                    template: '/templates/resultDialog.html',
                    className: 'ngdialog-theme-default'
                });
            }
        }
        else {
            $scope.setModalContent("Error occurred !");
            ngDialog.open({
                template: '/templates/resultDialog.html',
                className: 'ngdialog-theme-default'
            });
        }
    };
});