﻿'use strict';
app.controller('DashboardController', function DashboardController($scope, dashboardData) {
    dashboardData.getData()
        .success(function (dashboard) { $scope.dashboard = dashboard; });

    // Method which receives data.
    seatMessageHub.client.handleSeatMessage = function (message) {
        // Method which handles messages.
        $scope.receivedMessageHandler(message);
    };


    $scope.receivedMessageHandler = function (seatDataMessageJsonString) {
        var seatDataMessage = JSON.parse(seatDataMessageJsonString);
        if (seatDataMessage.DataProcessedSuccessfully) {
            switch (seatDataMessage.MessageType) {
                case 1: // Booking
                    $scope.dashboard.availableSeats -= 1;
                    break;
                case 2: // Releasing.
                    $scope.dashboard.availableSeats += 1;
                    break;
                default:
                    return;
            }
            $scope.$apply();
        }
    };

    
});