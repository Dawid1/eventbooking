﻿'use strict';
app.controller('RoomController', function EventController($scope, $location, $window, eventData, dialogService, ngDialog) {
    $scope.newRoom = {};
    $scope.newRoom.name = "";
    $scope.newRoom.rows = 1;
    $scope.newRoom.columns = 1;
    $scope.showRoomView = false;
    $scope.newRoom.seats = [];
    $scope.newRoom.eventId = $location.search().eventId;
    $scope.saving = false;

    $scope.getSeatClass = function (row, column) {
        var currentSeat = $scope.getCurrentSeat(column, row);
        if (currentSeat != null) {

            return "seat-available";

        }
        return "not-defined";
    };

    $scope.setModalContent = function (content) {
        dialogService.modalContent = content;
    };

    $scope.roomChanged = function () {
        $scope.showRoomView = false;
    };

    $scope.range = _.range;

    $scope.generateView = function () {
        $scope.newRoom.seats = [];
        for (var i = 1; i <= $scope.newRoom.rows; i++) {
            for (var j = 1; j <= $scope.newRoom.columns; j++) {
                $scope.newRoom.seats.push(
                    {
                        seatNumber: (((i - 1) * $scope.newRoom.columns) + j),
                        available: true,
                        row: i,
                        column: j

                    });
            }
        }
        $scope.showRoomView = true;
    };

    $scope.getSeatNumber = function (row, column) {
        var currentSeat = $scope.getCurrentSeat(column, row);
        if (currentSeat) {

            return currentSeat.seatNumber;

        }
        return "";

    };

    $scope.reorderSeats = function () {
        var counter = 1;
        var seats = [];

        for (var i = 1; i <= $scope.newRoom.rows; i++) {
            for (var j = 1; j <= $scope.newRoom.columns; j++) {
                var currentSeat = $scope.getCurrentSeat(j, i);
                if (currentSeat) {
                    seats.push({
                        seatNumber: counter,
                        available: true,
                        row: i,
                        column: j
                    });
                    counter++;
                }
            }
        }
        $scope.newRoom.seats = seats;
    };

    $scope.getCurrentSeat = function (column, row) {
        var room = $scope.newRoom;
        var allColumns = room.columns;
        var currentSeat = null;
        var count = room.seats.length;
        for (var i = 0; i < count; i++) {
            if (room.seats[i].column == column && room.seats[i].row == row) {
                currentSeat = room.seats[i];
                break;
            }
        }

        return currentSeat != null ? currentSeat : null;
    };

    $scope.createConference = function() {
        if (!$scope.newRoomForm.$valid) {
            return;
        }

        //$scope.saving = true;
        $scope.setModalContent("Saving...");
        ngDialog.open({
            template: '/templates/waitingDialog.html',
            className: 'ngdialog-theme-default',
            closeByEscape: false,
            showClose: false,
            closeByDocument: false
        });


        eventData.createConferenceRoom($scope.newRoom).success(function (data, status, headers, config) {
            ngDialog.closeAll();
            $scope.setModalContent("Operation has been done successfuly");
            ngDialog.openConfirm({
                template: '/templates/resultDialog.html',
                className: 'ngdialog-theme-default'
            }).then(function () {
                $window.location.href = '/events.html';
            }, function () {
                $window.location.href = '/events.html';
            });
        }).error(function (data, status, header, config) {
            ngDialog.closeAll();
            $scope.setModalContent("Operation error");
            ngDialog.openConfirm({
                template: '/templates/resultDialog.html',
                className: 'ngdialog-theme-default'
            }).then(function () {
                $window.location.href = '/events.html';
            }, function () {
                $window.location.href = '/events.html';
            });
        });
    };

    $scope.showConfirmDialog = function (row, column) {

        var currentSeat = $scope.getCurrentSeat(column, row);

        $scope.setModalContent("");

        if (currentSeat != null) {

            $scope.setModalContent("You ara defining this seat as a free space " + currentSeat.seatNumber + ". Please confirm.");

        }
        else {
            $scope.setModalContent("You ara defining this free space as a seat. Please confirm.");
        }

        ngDialog.openConfirm({
            template: '/templates/confirmDialog.html',
            className: 'ngdialog-theme-default'
        }).then(function () {
            if (currentSeat) {
                $scope.newRoom.seats.splice(_.indexOf($scope.newRoom.seats, currentSeat), 1);
            } else {
                $scope.newRoom.seats.push(
                    {
                        seatNumber: (((row - 1) * $scope.newRoom.columns) + column),
                        available: true,
                        row: row,
                        column: column

                    });
            }
        }, function () {
            
        });
    };
});