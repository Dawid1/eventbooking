﻿app.factory('eventData', function ($http) {
    return {
        getEvents: function () {
            return $http({ method: 'GET', url: 'api/event' });
        },
        bookSeat: function (conference, room, seat, callback) {
            var config = {
                headers: {
                    'Content-Type': 'application/json;'
                }
            };

            return $http.put('api/event/' + conference.id + '/room/' + room.id + '/seat', { SeatNumber: seat.seatNumber, Available: !seat.available, Row: seat.row, Column: seat.column, Id: seat.id }, config)
                .success(function (data, status, headers, config) {
                    callback("OK");
                })
            .error(function (data, status, header, config) {
                callback("Error");
            });
        },
        createConferenceRoom: function (room) {
            var config = {
                headers: {
                    'Content-Type': 'application/json;'
                }
            };

            return $http.post('api/event/' + room.eventId + '/CreateRoom', room, config);

        },
        getConferenceRoomForEdit: function (eventId, roomId) {

            return $http({ method: 'GET', url: 'api/event/'+ eventId + '/ConferenceRoomGet/'+ roomId });
        }
    }
});