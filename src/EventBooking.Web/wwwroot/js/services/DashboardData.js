﻿app.factory('dashboardData', function ($http) {
    return {
        getData: function () {
            return $http({ method: 'GET', url: 'api/dashboard' });
        }
    }
});