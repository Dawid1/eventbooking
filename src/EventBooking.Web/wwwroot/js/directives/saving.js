﻿angular.module('app').directive('saving', function () {
    return {
        templateUrl: "templates/directives/saving.html",
        restrict: "E",
        link: function (scope, element, attr) {
            scope.$watch('saving', function (val) {
                if (val)
                    $(element).show();
                else
                    $(element).hide();
            });
        }
    }
});