﻿using EventBooking.Server;
using EventBooking.Server.Db;
using EventBooking.Web.IoC;
using EventBooking.Web.SignalR;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StructureMap;
using System;

namespace EventBooking.Web
{
    public class Startup
    {
        private IServiceProvider _serviceProvider;
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddControllersAsServices();

            //var connection = @"Server=(localdb)\mssqllocaldb;Database=EventBooking;Trusted_Connection=True;";
            //services.Add.AddEntityFramework().AddDbContext<EventBookingContext>(options => options.UseSqlServer(connection));

            _serviceProvider = ConfigureIoC(services);

            return _serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseDefaultFiles();

            app.UseStaticFiles();

            app.UseSignalR2(_serviceProvider);

            app.UseMvc(config =>
            {
                config.MapRoute("api", "api/{controller}/{action}/{id?}", new { controller = "Dashboard", action = "Get" });
            });
        }

        public IServiceProvider ConfigureIoC(IServiceCollection services)
        {
            var container = new Container();

            container.Configure(config =>
            {
                // Register stuff in container, using the StructureMap APIs...
                config.Scan(scan =>
                {
                    scan.AssemblyContainingType(typeof(Startup));
                    scan.WithDefaultConventions();
                });

                config.For<IDependencyResolver>().Singleton().Use<StructureMapSignalRDependencyResolver>();

                config.For<IConnectionManager>().Use<ConnectionManager>();

        //        config.For<IHubConnectionContext<dynamic>>().Use(ctx => ctx.GetInstance<IDependencyResolver>()
        //.Resolve<IConnectionManager>()
        //.GetHubContext<SeatMessageHub>().Clients);

                config.For<SeatMessageHub>().Use<SeatMessageHub>();

                config.AddRegistry<EventBookingServerRegistry>();

                //Populate the container using the service collection
                config.Populate(services);
            });

            return container.GetInstance<IServiceProvider>();
        }
    }
}
