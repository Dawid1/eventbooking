﻿using Microsoft.AspNetCore.Mvc;
using EventBooking.Server.Events;

namespace EventBooking.Web.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IEventService _eventService;
        public DashboardController(IEventService eventService)
        {
            _eventService = eventService;
        }
        
        // GET: api/dashboard
        [HttpGet]
        public DashboardDto Get()
        {
            return _eventService.GetDashboardDto();
        }
    }
}
