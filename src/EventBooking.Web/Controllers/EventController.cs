﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using EventBooking.Server.Events;
using System;
using EventBooking.Server.Seats;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace EventBooking.Web.Controllers
{
    [Route("api/[controller]")]
    public class EventController : Controller
    {
        private readonly IEventService _eventService;

        public EventController(IEventService eventServcie)
        {
            _eventService = eventServcie;
        }
        
        // GET: api/event
        [HttpGet]
        public IEnumerable<EventDto> Get()
        {
            var events = _eventService.GetEvents().ToList();

            return events;
        }

        // GET api/event/name
        [HttpGet("{name}")]
        public EventDto Get(string name)
        {
            return _eventService.GetEventByName(name);
        }

        [HttpPost()]
        [Route("{eventId?}/CreateRoom")]
        public void CreateRoom(Guid eventId, [FromBody] ConferenceRoomDto room)
        {
            _eventService.CreateConferenceRoom(room);
        }

        [HttpGet()]
        [Route("{eventId?}/ConferenceRoomGet/{roomId?}")]
        public ConferenceRoomDto ConferenceRoomGet(Guid eventId, Guid roomId)
        {
            return _eventService.ConferenceRoomGet(eventId, roomId);
        }
    }
}
